/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicioslogic2joseignacionavassanz;

/**
 *
 * @author Jose Ignacio Navas Sanz CURSO: 1º DAM ASIGNATURA: PROGRAMACIÓN
 *
 * Ejercicios CodingBat - Logic2 Serie de ejercicios de la Web CodingBat
 * consistentes en programas cortos en los que debemos elaborar una cadena de
 * pruebas
 *
 * Cada programa es diferente. En este caso, son 9 programas en lenguaje Java.
 */
public class EjerciciosLogic2JoseIgnacioNavasSanz {

    /*
    Ejercicio 1 - CodingBatLogic2 - makeBricks
    Queremos hacer una fila de ladrillos. Tenemos dos tipos de
    ladrillos, small, que miden 1cm y large, que miden 5cm. 
    Si la suma del número de ladrillos es igual o superior
    a la longitud del muro deseado devolverá true. En caso
    contrario, devolerá false.
     */
    public boolean makeBricks(int small, int big, int goal) {
        /*
        Si la suma de ladrillos pequeños
        y ladrillos grandes por su valor
        es menor que el objetivo, será false 
         */
        if (small + 5 * big < goal) {
            return false;
        }
        /*
        Si el resto de dividir el objetivo 
        entre 5 supera el número de ladrillos
        pequeños, devolverá false al no poder
        usar todos los ladrillos disponibles
         */
        if (goal % 5 > small) {
            return false;
        } /*
        En caso contrario, devolverá true
         */ else {
            return true;
        }
    }

    /*
    Ejercicio 2 - CodignBat - loneSum
    El ejercicio recibe tres números enteros y
    suma todos. Si los numeros son distintos, la
    suma será la suma de todos los números. En caso
    de haber algún número repetido, sólo sumará los
    números que no estén repetidos.
     */
    public int loneSum(int a, int b, int c) {
        /*
        Si A es distinto de B y distinto de C
        devuelve la suma de los 3 números
         */
        if (a != b && a != c && b != c) {
            return a + b + c;
        }
        /*
        Si A no es igual a B, y B es igual a C,
        A, será distinto de B o de C puesto que
        son iguales y devolverá A
         */
        if (a != b && b == c) {
            return a;
        }
        /*
        Si B no es igual a C, y A es igual a C,
        B, será distinto de A o de C puesto que
        son iguales y devolverá B
         */
        if (b != c && a == c) {
            return b;
        }
        /*
        Si C no es igual a A, y A es igual a B,
        C, será distinto de A o de B puesto que
        son iguales y devolverá C
         */
        if (c != a && a == b) {
            return c;
        } //SI NO SE DA NADA DE ESTO, DEVUELVE 0
        else {
            return 0;
        }
    }

    /*
    Ejercicio 3 - CodingBatLogic2 - luckySum
    Dados tres valores enteros, devuelve su suma.
    Sin embargo, si uno de esos valores es 13, ese
    valor no contará y los de su derecha tampoco.
     */
    public int luckySum(int a, int b, int c) {
        /*
        Si A es distinto de B, B es distinto de C, y por
        tanto, C es distinto de A, y además cada número
        es distinto de 13, devuelve la suma total
         */
        if (a != b && a != 13 && a != c && c != 13 && b != c && b != 13) {
            return a + b + c;
        }
        /*
        En cuanto A sea 13, B no contará, ahí entra en juego
        el valor de C. En parte, que C sea 13 da igual, de
        ahí que esté comentado, si C es 13 nunca se tendrá en
        cuenta y no afectará a nadie por no tener ningún valor
        a su derecha. Si A es 13, B no cuenta, pero además, si
        B también es 13, C tampoco contará y por tanto el 
        valor que devolverá es 0
         */
        if (a == 13 /* && c==13 */ || a == 13 && b == 13) {
            return 0;
        }
        /*
        Si B es 13, C no cuenta y por tanto devolvemos A
         */
        if (b == 13) {
            return a;
        }
        /*
        Si C es 13, C no cuenta y devolvemos A+B siempre y 
        cuando A o B o ambas, no sean 13 y sean distintas
         */
        if (c == 13) {
            return a + b;
        } else {
            return 0;
        }
    }

    /*
    Ejercicio 4 - CodingBatLogic2 - noTeenSum
    dados 3 valores enteros, A, B y C y devuelve su suma.
    Si cualquiera de los valores está entre [13 y 19] ese
    valor vale 0, a excepción de 15 y 16 que no cuentan para
    este rango.
    Escripe un método separado denominado public int fixTeen(int n)
    que recibe un valor entero y devuelve el valor para la regla
    del rango. Con esto conseguirás evitar repetir el código 3 veces.
     */
    public int noTeenSum(int a, int b, int c) {
        return fixTeen(a) + fixTeen(b) + fixTeen(c);
    }

    /*
    Si N es mayor o igual que 13 y menor o igual que 19, es decir,
    está en el rango que vale 0 valdrá 0 a excepción de si es 15
    o 16 que valdrá su valor. Si no está entre los valores del
    rango el número valdrá su valor original y se sumarán
     */
    public int fixTeen(int n) {
        if (n >= 13 && n <= 19 || n != 15 || n != 16) {
            return 0;
        } else {
            return n;
        }
    }

    /*
    Ejercicio 5 - CodingBatLogic2 - roundSum
    Dados 3 eneteros, redondeamos el número en función de la cifra
    en la que acabe. Si el número acaba en 5 o más redondearemos 
    hacia arriba (5 al 10, 15 al 20... y así sucesivamente). Si este
    número es menos de 5, redondearemos hacia abajo (4 al 0, 12 al 10
    y así sucesivamente).
    Se recomienta la creción de un método de apoyo al igual que en
    el ejercicio anterior. Este método debe denominarse
    public in num10(int num) y deberemos llamarlo 3 ocasiones.
     */
    public int roundSum(int a, int b, int c) {
        return round10(a) + round10(b) + round10(c);
    }

    //Método de apoyo
    /*
    Si el resto del número dividido entre 10 es mayor que 5
    (Por ejemplo, 27 entre 10, el resto es 7). En este caso,
    devolveremos el número (27) + 10 - resto para llegar a 30.
    
    Si no es mayor o igual que 5, por ejemplo 23, su resto
    es 3, restamos al número el resto.
     */
    public int round10(int num) {
        if (num % 10 >= 5) {
            return num + (10 - (num % 10));
        } else {
            return num - (num % 10);
        }
    }

    /*
    Ejercicio 6 - CodingBatLogic2 - closeFar
    Dados 3 enteros A, B y C, devuelve true
    si B difiere en 1 o menos de A mientras
    que C difere en dos o más tanto de B como
    de A.
    
    Del mismo modo, si C difere de 1 o menos de
    A, mientras que B difiere en 2 o más tanto
    de C como de A, devolverá true.
    
    En cualquier otro caso, dará falso.
    
    Utilizamos la funcion Math.abs(int n) para
    que la resta nos devuelva el valor absoluto
    del número y no nos devuelva valores negativos
     */
    public boolean closeFar(int a, int b, int c) {
        //Comparación de B con A - Comparación de C con A y B con C
        if (Math.abs(b - c) >= 2 && Math.abs(a - c) >= 2 && Math.abs(a - b) <= 1) {
            return true;
        }
        //Comparación de C con A - Comparación de C con B y B con A
        if (Math.abs(b - c) >= 2 && Math.abs(a - c) <= 1 && Math.abs(a - b) >= 2) {
            return true;
        } //En cualquier otro caso, devuelve falso
        else {
            return false;
        }
    }

    /*
    Ejercicio 7 - CodingBatLogic2 - blackjack
    Dados dos enteros, A y B mayores que 0,
    devuelve el valor más cercano a 21 sin
    pasarse de 21.
     */
    public int blackjack(int a, int b) {
        /*Si nos pasamos de 21 
        en los dos numeros 
        devolverá 0
         */
        if (a > 21 && b > 21) {
            return 0;
        } /*
        Si B es mayor que 21,
        devolverá A
         */ else if (b > 21) {
            return a;
        } /*
        Si A es mayor que 21,
        devolverá B
         */ else if (a > 21) {
            return b;
        } /*
        Si B es mayor que A, y
        además B es menor o
        igual que 21 devuelve B
         */ else if (b > a && b <= 21) {
            return b;
        } /*
        En cualquier otro caso,
        devolverá el valor de A
         */ else {
            return a;
        }
    }

    /*
    Ejercicio 8 - CodingBatLogic2 - evenlySpaced
    Dados tres enteros A, B y C, uno es el
    pequeño, otro el mediano y otro el grande
    devuelve true si están lo suficientemente 
    espaciados los unos de los otros. Es decir,
    si la diferencia entre el pequeño y el
    mediano es la misma que entre el mediano
    y el grande. Si no, devolverá false.
    
    Utilizamos la funcion Math.abs(int n) para
    que la resta nos devuelva el valor absoluto
    del número y no nos devuelva valores negativos
     */
    public boolean evenlySpaced(int a, int b, int c) {
        /*
        Si A == B y B == C C será igual a A y
        habrá la misma distancia entre los 3,
        por tanto devolvemos true cada vez
        que los 3 números sean iguales.
         */
        if (a == b && b == c) {
            return true;
        }
        /*
        Segundo, comparamos los números para ver cual
        es el mayor, cual es mediano y cual pequeño
         */
 /*
        Si A<B y B<C el más pequeño es A, el mediano
        es B y el grande es C EJ: (1,2,3). Si la 
        distancia entre A y B es igual a la distancia 
        entre b y c devolveremos true
         */
        if (a < b && b < c && Math.abs(a - b) == Math.abs(b - c)) {
            return true;
        }
        /*
        Si B<C y C<A el más pequeño será B, el mediano
        será C y el grande será A. Por tanto, si la
        distancia entre B y C es similar a la distancia
        entre A y C devolveremos true
         */
        if (b < c && c < a && Math.abs(b - c) == Math.abs(c - a)) {
            return true;
        }
        /*
        Si C<A y A<B el más pequeño es C, el mediano será
        A y el más grande será B. Por tanto, si la distancia
        entre C y A es similar entre la distancia de B y A
        devolveremos true.
         */
        if (c < a && a < b && Math.abs(c - a) == Math.abs(b - a)) {
            return true;
        }
        //Caso especial - A>B y B<C 
        /*
        Si A>B y B<C el más pequeño será B, el mediano A y
        el más grande será C. Por tanto, si la distancia 
        entre A y B es similar a la distancia entre C y A
        devolveremos true.
         */
        if (a > b && b < c && Math.abs(a - b) == Math.abs(c - a)) {
            return true;
        } else {
            return false;
        }
    }

    /*
    Ejercicio 9 - CodingBatLogic2 - makeChocolate
    Dados tres enteros A, B y C se desean hacer
    kilos de chocolate. El A vale un kilo, mientras
    que B vale 5 kilos. C es el objetivo de kilos
    de chocolate a hacer. Si no podemos llegar
    al objetivo devolveremos .1. Siempre usaremos
    las barras de 5 kilos primero, y en caso de
    quedarnos cortos usaremos las barras pequeñas.
    Devolvemos 0 si no usamos barras pequeñas,
    -1 si no llegamos al objetivo, o el número de
    barras pequeñas que debemos utilizar.
     */
    public int makeChocolate(int small, int big, int goal) {
        /*
        Si llegamos al objetivo con las barras
        de 5 kilos, devolvemos 0 ya que no 
        usamos barras pequeñas
         */
        if (big <= 1 && goal == 5 || goal == 5 * big) {
            return 0;
        }
        /*
        Si la suma de las grandes y las pequeñas no
        llega al objetivo devolveremos -1.
        Si además, tenemos más de 1 barra grande, las
        cuales usamos primero, y el objetivo es menor
        que la suma de las barras grandes, debemos usar
        1 barra grande y las pequeñas. Si no llegamos
        con las barras pequeñas, devolvemos -1.
         */
        if (5 * big + small < goal || big >= 2 && small == 1 && 5 * (big / big) + small < goal) {
            return -1;
        }
        /*
        Si el objetivo supera el uso de barras grandes
        devolvemos el número de barras pequeñas a usar
        que es el objetivo - barras grandes usadas
         */
        if (goal > 5 * big) {
            return goal - 5 * big;
        }
        /*
        Si tenemos más de dos barras grandes, devolvemos
        el objetivo restándole 5 kilos por cada barra, si
        tenemos 8 barras, restará 40 kilos. Y así nos
        dará el número de barras pequeñas a usar
         */
        if (big >= 2) {
            return goal - (5 * (big / big));
        } /*
        En cualquier otro caso, devolveremos el número
        de barras pequeñas que tenemos, que será el que
        vayamos a emplear para hacer el chocolate
         */ 
        else {
            return small;
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
    }
}
